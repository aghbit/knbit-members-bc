FROM java:8

RUN mkdir /opt/knbit-members-bc

WORKDIR /opt/knbit-members-bc

ADD ./rest/build/libs/rest-1.0-SNAPSHOT-standalone.jar ./app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]

CMD ["docker"]