package pl.edu.bit.membership.domain.aggregate

import akka.actor._
import akka.contrib.pattern.ShardRegion.Passivate
import pl.edu.bit.membership.domain.aggregate.Member._
import pl.edu.bit.membership.domain.command.{AcceptDeclaration, RejectDeclaration, SubmitDeclaration}
import pl.edu.bit.membership.domain.event.{InvalidActionEvent, DeclarationAccepted, DeclarationRejected, DeclarationSubmitted}
import pl.edu.bit.membership.domain.valueobject.Declaration
import pl.edu.bit.membership.domain.valueobject.id.UserId
import pl.edu.bit.membership.external.aa.AuthService
import pl.edu.bit.membership.infrastructure.akka.persistence.PersistentActor
import pl.edu.bit.membership.infrastructure.net.spray.RequestContext

import scala.concurrent.duration._

/**
 * @author Wojciech Milewski
 */
class Member(implicit authService: AuthService) extends PersistentActor with ActorLogging {
  context.setReceiveTimeout(10.minutes)

  private var memberType: Type = Candidate

  override def persistenceId: String = self.path.parent.name + "-" + self.path.name

  override def receiveRecover: Receive = {
    case ev:DeclarationSubmitted =>
      memberType = WaitingMember(Declaration.from(ev))
  }

  override def receiveCommand: Receive = {
    /**
     * Submit declaration process
     */
    case cmd: SubmitDeclaration if memberType == Candidate =>
      persist(DeclarationSubmitted(cmd.userId, cmd.email, cmd.firstName, cmd.lastName, cmd.departmentName, cmd.indexNumber, cmd.startOfStudiesYear)) {
        event =>
          log.info(s"Declaration submitted for ${cmd.userId}")
          memberType = WaitingMember(Declaration.from(event))
          publish(event)
      }
    case cmd: SubmitDeclaration =>
      log.info(s"Declaration cannot be submitted if user ${cmd.userId} is $memberType")

    /**
     * Accept declaration process
     */
    case cmd: AcceptDeclaration => memberType match {
      case WaitingMember(declaration) =>
        import cmd.requestContext
        activateAccount(cmd.userId, declaration)
      case _ =>
        val msg = s"User ${cmd.userId} is in incorrect state. Should be WaitingMember, but is $memberType"
        log.info(msg)
        sender() ! InvalidActionEvent(msg)
    }

    /**
     * Reject declaration process
     */
    case cmd: RejectDeclaration => memberType match {
      case WaitingMember(declaration) =>
        val s: ActorRef = sender()
        persist(DeclarationRejected(cmd.userId)) {
          event =>
            log.debug(s"Declaration rejected for ${cmd.userId}")
            memberType = Candidate
            publish(event)
            s ! event
        }
      case _ =>
        log.debug(s"User ${cmd.userId} is in incorrect state. Should be Candidate, but is $memberType")
    }
    case ReceiveTimeout => context.parent ! Passivate(stopMessage = PoisonPill)

  }

  private def activateAccount(userId: UserId, declaration: Declaration)(implicit requestContext: RequestContext): Unit = {
    import context.dispatcher

    val s: ActorRef = sender()

    authService.activateAccount(userId) onSuccess {
      case AuthService.UserActivated(_) => persistOnActorThread(DeclarationAccepted(userId)) {
        event =>
          log.debug(s"Declaration accepted for $userId")
          memberType = ActiveMember(declaration)
          publish(event)
          s ! event
      }
      case response =>
        log.debug(s"Activation rejected: $response")
    }

  }
}

object Member {

  def props(implicit authService: AuthService): Props = Props(new Member)

  sealed trait Type

  case object Candidate extends Type

  case class WaitingMember(declaration: Declaration) extends Type

  case class ActiveMember(declaration: Declaration) extends Type

  case class InactiveMember(declaration: Declaration) extends Type

}
