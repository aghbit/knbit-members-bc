package pl.edu.bit.membership.domain.command

import pl.edu.bit.membership.domain.valueobject.id.UserId
import pl.edu.bit.membership.domain.valueobject.{IndexNumber, Password, Email}
import pl.edu.bit.membership.infrastructure.net.spray.RequestContext

/**
 * @author Wojciech Milewski
 */
sealed trait Command
case class CreateAccount(email: Email, password: Password) extends Command
case class SubmitDeclaration(
                              userId: UserId,
                              email: Email,
                              firstName: String,
                              lastName: String,
                              departmentName: String,
                              indexNumber: IndexNumber,
                              startOfStudiesYear: Int) extends Command with HasUserId
case class AcceptDeclaration(userId: UserId)(implicit val requestContext: RequestContext) extends Command with HasUserId
case class RejectDeclaration(userId: UserId) extends Command with HasUserId

trait HasUserId {
  val userId: UserId
}