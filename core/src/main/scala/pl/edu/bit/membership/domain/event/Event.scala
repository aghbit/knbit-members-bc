package pl.edu.bit.membership.domain.event

import java.time.{Month, LocalDate, LocalDateTime}

import pl.edu.bit.membership.domain.valueobject.{IndexNumber, Email}
import pl.edu.bit.membership.domain.valueobject.id.UserId

/**
 * @author Wojciech Milewski
 */
sealed trait Event

sealed trait AccountCreationEvent extends Event
case class AccountCreated(email: Email, userId: UserId) extends AccountCreationEvent
case class AccountRejected(email: Email, reason: AccountRejected.Reason) extends AccountCreationEvent
object AccountRejected {
  sealed trait Reason
  case object InvalidEmail extends Reason
  case object PasswordTooWeak extends Reason
  case object UserExists extends Reason
}

case class DeclarationSubmitted(
                                 userId: UserId,
                                 email: Email,
                                 firstName: String,
                                 lastName: String,
                                 departmentName: String,
                                 indexNumber: IndexNumber,
                                 startOfStudiesYear: Int,
                                 submitDate: LocalDate = LocalDate.now(),
                                 expirationDate: LocalDate = LocalDate.of(2016, 10, 31)
                                 ) extends Event

sealed trait DeclarationConfirmationEvent extends Event
case class DeclarationAccepted(userId: UserId) extends DeclarationConfirmationEvent
case class DeclarationRejected(userId: UserId) extends DeclarationConfirmationEvent

case class InvalidActionEvent(msg: String) extends Event