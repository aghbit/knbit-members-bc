package pl.edu.bit.membership.domain.office

import akka.actor.{ActorRef, ActorSystem}
import akka.contrib.pattern.{ClusterSharding, ShardRegion}
import pl.edu.bit.membership.domain.command.CreateAccount
import pl.edu.bit.membership.domain.service.Admin
import pl.edu.bit.membership.external.aa.AuthService

/**
 * @author Wojciech Milewski
 */
object AdminOffice {
  private val adminIdExtractor: ShardRegion.IdExtractor = {
    case cmd: CreateAccount => ("admin-service", cmd)
  }

  private val adminShardResolver: ShardRegion.ShardResolver = {
    case cmd: CreateAccount => "admin-service"
  }

  def apply()(implicit system: ActorSystem, authService: AuthService): ActorRef = {
    ClusterSharding(system).start(
      "Admin",
      Some(Admin.props),
      adminIdExtractor,
      adminShardResolver
    )
  }
}
