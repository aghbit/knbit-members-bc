package pl.edu.bit.membership.domain.office

import akka.actor._
import akka.contrib.pattern.{ClusterSharding, ShardRegion}
import pl.edu.bit.membership.domain.aggregate.Member
import pl.edu.bit.membership.domain.command._
import pl.edu.bit.membership.external.aa.AuthService

/**
 * @author Wojciech Milewski
 */
object MemberOffice {

  private val idExtractor: ShardRegion.IdExtractor = {
    case cmd: SubmitDeclaration => (cmd.userId.id, cmd)
    case cmd: AcceptDeclaration => (cmd.userId.id, cmd)
    case cmd: RejectDeclaration => (cmd.userId.id, cmd)
  }

  private val shardResolver: ShardRegion.ShardResolver = {
    case cmd: SubmitDeclaration => cmd.userId.id
    case cmd: AcceptDeclaration => cmd.userId.id
    case cmd: RejectDeclaration => cmd.userId.id
  }

  def apply()(implicit system: ActorSystem, authService: AuthService): ActorRef = {
    ClusterSharding(system).start(
      "Member",
      Some(Member.props),
      idExtractor,
      shardResolver
    )
  }
}