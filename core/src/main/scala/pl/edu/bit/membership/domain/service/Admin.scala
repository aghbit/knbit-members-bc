package pl.edu.bit.membership.domain.service

import akka.actor._
import akka.contrib.pattern.ShardRegion.Passivate
import pl.edu.bit.membership.domain.command.CreateAccount
import pl.edu.bit.membership.domain.event.{AccountCreated, AccountRejected}
import pl.edu.bit.membership.domain.valueobject.{Email, Password}
import pl.edu.bit.membership.external.aa.AuthService
import pl.edu.bit.membership.external.aa.AuthService.CreateUserResponse
import pl.edu.bit.membership.infrastructure.akka.persistence.PersistentActor

import scala.concurrent.Future
import scala.concurrent.duration._

/**
 * Admin Domain Service. Stateless object providing domain business logic disconnected to any aggregate.
 *
 * Admin domain service is implemented as PersistentActor.
 *
 * Logically, there's only one instance of Admin domain service - no need of unique AdminID.
 * Technically, multiple actors can represent this service,
 * as it is stateless and methods can be called simultaneously.
 * Actually, it is advised to create new Admin actor if any other actor wants to use it.
 *
 * @author Wojciech Milewski
 */
class Admin(authService: AuthService) extends PersistentActor with ActorLogging {

  context.setReceiveTimeout(10.minutes)

  override def persistenceId: String = self.path.parent.name + "-" + self.path.name

  override def receiveRecover: Receive = {
    case AccountCreated => //do absolutely nothing
  }

  override def receiveCommand: Receive = {
    case CreateAccount(email, password) =>
      log.info(s"Command received: Create account for $email")
      createAccount(email, password)
    case ReceiveTimeout => context.parent ! Passivate(stopMessage = PoisonPill)
  }

  /**
   * Creates new actor responsible for sending CreateAccount request to AA server
   */
  def createAccount(email: Email, password: Password): Unit = {
    import context.dispatcher

    val s: ActorRef = sender()

    val response: Future[CreateUserResponse] = authService.createAccount(email, password)
    response onSuccess {
      case AuthService.AccountCreated(_, userId) =>
        log.info("account created but pre persisted")
        persistOnActorThread(AccountCreated(email, userId)) {
          event => {
            log.info(s"AccountCreated event with $email persisted")
            s ! event
            stopActor()
          }
        }
      case AuthService.InvalidEmail(_) =>
        log.info(s"$email is invalid")
        s ! AccountRejected(email, AccountRejected.InvalidEmail)
        stopActor()
      case AuthService.PasswordTooWeak(_, _) =>
        log.info(s"$password for $email is too weak")
        s ! AccountRejected(email, AccountRejected.PasswordTooWeak)
        stopActor()
      case AuthService.UserExists(_) =>
        log.info(s"user with $email already exists")
        s ! AccountRejected(email, AccountRejected.UserExists)
        stopActor()
      case serviceUnavailable@AuthService.ServiceUnavailable =>
        log.info("AA service unavailable")
        s ! serviceUnavailable
    }
  }
}

object Admin {
  def props(implicit authService: AuthService): Props = Props(new Admin(authService))
}