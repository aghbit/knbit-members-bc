package pl.edu.bit.membership.domain.valueobject

import java.time.LocalDate

import pl.edu.bit.membership.domain.event.DeclarationSubmitted
import pl.edu.bit.membership.domain.valueobject.id.UserId

/**
 * @author Wojciech Milewski
 */
case class Declaration(
                        userId: UserId,
                        email: Email,
                        firstName: String,
                        lastName: String,
                        departmentName: String,
                        indexNumber: IndexNumber,
                        startOfStudiesYear: Int,
                        submitDate: LocalDate,
                        expirationDate: LocalDate
                        )

object Declaration {
  def from(ev: DeclarationSubmitted): Declaration =
    Declaration(ev.userId, ev.email, ev.firstName, ev.lastName, ev.departmentName, ev.indexNumber, ev.startOfStudiesYear, ev.submitDate, ev.expirationDate)
}
