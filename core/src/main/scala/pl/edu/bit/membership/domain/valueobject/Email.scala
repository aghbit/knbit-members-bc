package pl.edu.bit.membership.domain.valueobject

import org.apache.commons.validator.routines.EmailValidator

/**
 * @author Wojciech Milewski
 */
case class Email(value: String) extends AnyVal {
  def isValid = EmailValidator.getInstance().isValid(value)
}
