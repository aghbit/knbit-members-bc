package pl.edu.bit.membership.domain.valueobject

import scala.util.Try

/**
 * @author Wojciech Milewski
 */
case class IndexNumber(value: String) extends AnyVal {
  def isValid = Try(value.toInt).filter(value => value > 0).isSuccess
}
