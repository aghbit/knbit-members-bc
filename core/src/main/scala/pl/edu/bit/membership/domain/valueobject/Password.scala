package pl.edu.bit.membership.domain.valueobject

/**
 * @author Wojciech Milewski
 */
case class Password(value: String) extends AnyVal {
}
