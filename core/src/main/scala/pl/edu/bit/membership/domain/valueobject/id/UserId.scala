package pl.edu.bit.membership.domain.valueobject.id

/**
 * @author Wojciech Milewski
 */
case class UserId(id: String)
