package pl.edu.bit.membership.external.aa

import pl.edu.bit.membership.domain.valueobject.id.UserId
import pl.edu.bit.membership.domain.valueobject.{Email, Password}
import pl.edu.bit.membership.external.aa.AuthService.{ActivateUserResponse, CreateUserResponse}
import pl.edu.bit.membership.infrastructure.net.spray.RequestContext

import scala.concurrent.Future

/**
 * @author Wojciech Milewski
 */
trait AuthService {
  def createAccount(email: Email, password: Password): Future[CreateUserResponse]
  def activateAccount(userId: UserId)(implicit requestContext: RequestContext): Future[ActivateUserResponse]
}

object AuthService {
  sealed trait CreateUserResponse
  case class AccountCreated(email: Email, userId: UserId) extends CreateUserResponse
  case class UserExists(email: Email) extends CreateUserResponse
  case class InvalidEmail(email: Email) extends CreateUserResponse
  case class PasswordTooWeak(email: Email, password: Password) extends CreateUserResponse

  sealed trait ActivateUserResponse
  case class UserActivated(userId: UserId) extends ActivateUserResponse
  case class NoSuchUser(userId: UserId) extends ActivateUserResponse

  case object ServiceUnavailable extends CreateUserResponse with ActivateUserResponse
}
