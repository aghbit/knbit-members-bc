package pl.edu.bit.membership.external.aa

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import pl.edu.bit.membership.domain.valueobject.id.UserId
import pl.edu.bit.membership.domain.valueobject.{Email, Password}
import pl.edu.bit.membership.external.aa.AuthService.{ActivateUserResponse, CreateUserResponse}
import pl.edu.bit.membership.external.aa.json.GenericJsons.BadRequestResponseJson
import pl.edu.bit.membership.external.aa.json.Protocol._
import pl.edu.bit.membership.external.aa.json.{ActivateUserRequest, PostUserNegativeResponse, PostUserPositiveResponse, PostUserRequest}
import pl.edu.bit.membership.infrastructure.net.spray.HttpResponseToResponseConversions._
import pl.edu.bit.membership.infrastructure.net.spray.RequestContext
import spray.client.pipelining._
import spray.httpx.SprayJsonSupport._

import scala.concurrent.Future

/**
 * Responsible for HTTP communication with AA microservice.
 * @author Wojciech Milewski
 */
class HttpAuthService(address: String)(implicit actorSystem: ActorSystem) extends AuthService with LazyLogging {

  override def createAccount(email: Email, password: Password): Future[CreateUserResponse] = {
    import actorSystem.dispatcher

    val pipeline = sendReceive

    pipeline(Post(s"$address/users", PostUserRequest(email.value, password.value))) map {
      case response if response.status.isSuccess => response.as[PostUserPositiveResponse] match {
        case Some(PostUserPositiveResponse(_, userId)) => AuthService.AccountCreated(email, UserId(userId))
        case None => throw new IllegalArgumentException("Can't convert response to PostUserPositiveResponse. API has changed?")
      }
      case response => response.as[PostUserNegativeResponse] match {
        case Some(PostUserNegativeResponse("USER_EXISTS")) => AuthService.UserExists(email)
        case Some(PostUserNegativeResponse("INVALID_EMAIL")) => AuthService.InvalidEmail(email)
        case Some(PostUserNegativeResponse("WEAK_PASSWORD")) => AuthService.PasswordTooWeak(email, password)
        case _ =>
          logger.warn("AA create user: unknown negative response")
          AuthService.ServiceUnavailable
      }
    } recover {
      case e =>
        logger.error("AA create user: request wasn't successful", e)
        AuthService.ServiceUnavailable
    }
  }

  override def activateAccount(userId: UserId)(implicit requestContext: RequestContext): Future[ActivateUserResponse] = {
    import actorSystem.dispatcher

    val pipeline = addHeader(RequestContext.TOKEN_HEADER, requestContext.authToken) ~> sendReceive

    pipeline(Patch(s"$address/users/${userId.id}/active", ActivateUserRequest(true))) map {
      case response if response.status.isSuccess => AuthService.UserActivated(userId)
      case response => response.as[BadRequestResponseJson] match {
        case Some(BadRequestResponseJson("NO_SUCH_USER")) => AuthService.NoSuchUser(userId)
        case _ =>
          logger.warn("AA activate account: unknown negative response")
          AuthService.ServiceUnavailable
      }
    } recover { case e =>
        logger.error("AA activate account: request wasn't successful", e)
      AuthService.ServiceUnavailable
    }
  }
}

object HttpAuthService {
  def apply(address: String)(implicit actorSystem: ActorSystem) = new HttpAuthService(address)
}