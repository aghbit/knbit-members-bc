package pl.edu.bit.membership.external.aa

import java.util.UUID

import com.typesafe.scalalogging.StrictLogging
import pl.edu.bit.membership.domain.valueobject.{Password, Email}
import pl.edu.bit.membership.domain.valueobject.id.UserId
import pl.edu.bit.membership.external.aa.AuthService.{UserActivated, AccountCreated, CreateUserResponse, ActivateUserResponse}
import pl.edu.bit.membership.infrastructure.net.spray.RequestContext

import scala.concurrent.Future

/**
 * @author Wojciech Milewski
 */
class MockedAuthService extends AuthService with StrictLogging {
  override def createAccount(email: Email, password: Password): Future[CreateUserResponse] = {
    logger.debug(s"Dummy create account for $email")
    Future.successful(AccountCreated(email, UserId(UUID.randomUUID.toString)))
  }


  override def activateAccount(userId: UserId)(implicit requestContext: RequestContext): Future[ActivateUserResponse] = {
    logger.debug(s"Dummy activate account for $userId")
    Future.successful(UserActivated(userId))
  }

}

object MockedAuthService {
  def apply() = new MockedAuthService
}
