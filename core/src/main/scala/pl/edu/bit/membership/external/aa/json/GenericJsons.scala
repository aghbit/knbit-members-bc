package pl.edu.bit.membership.external.aa.json

/**
 * @author Wojciech Milewski
 */
object GenericJsons {
  case class BadRequestResponseJson(reason: String)
}
