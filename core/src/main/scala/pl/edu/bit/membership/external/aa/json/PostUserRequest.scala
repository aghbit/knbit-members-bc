package pl.edu.bit.membership.external.aa.json

/**
 * @author Wojciech Milewski
 */
private[aa] case class PostUserRequest(email: String, password: String)

private[aa] case class PostUserPositiveResponse(email: String, userId: String)
private[aa] case class PostUserNegativeResponse(reason: String)

private[aa] case class ActivateUserRequest(active: Boolean)