package pl.edu.bit.membership.external.aa.json

import pl.edu.bit.membership.external.aa.json.GenericJsons.BadRequestResponseJson
import spray.json.DefaultJsonProtocol

/**
 * @author Wojciech Milewski
 */
private[aa] object Protocol extends DefaultJsonProtocol {
  implicit val postUserRequestFormat = jsonFormat2(PostUserRequest)
  implicit val postUserPositiveResponse = jsonFormat2(PostUserPositiveResponse)
  implicit val postUserNegativeResponse = jsonFormat1(PostUserNegativeResponse)

  implicit val activateUserRequest = jsonFormat1(ActivateUserRequest)

  implicit val genericBadRequestResponseJson = jsonFormat1(BadRequestResponseJson)
}
