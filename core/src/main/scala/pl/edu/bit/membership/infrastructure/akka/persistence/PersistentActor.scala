package pl.edu.bit.membership.infrastructure.akka.persistence

import akka.actor.Actor.Receive
import pl.edu.bit.membership.infrastructure.akka.persistence.PersistentActor.{PersistSingleEvent, DefaultReceive}

/**
 * @author Wojciech Milewski
 */
trait PersistentActor extends akka.persistence.PersistentActor {
  private def receivePersistCommand[T]: Receive = {
    case PersistSingleEvent(event: T, handler: (T => Unit)) => persist(event)(handler)
  }
  def receiveMessage: Receive = DefaultReceive

  override def receive: Receive = receivePersistCommand orElse receiveMessage orElse receiveCommand

  def publish(event: AnyRef): Unit = context.system.eventStream.publish(event)

  def stopActor(): Unit = context.stop(context.self)
  
  def persistOnActorThread[A](event: A)(handler: A => Unit) = {
    context.self ! PersistSingleEvent(event, handler)
  }
}

object PersistentActor {
  private object DefaultReceive extends Receive {
    override def isDefinedAt(x: Any): Boolean = false

    override def apply(v1: Any): Unit = ???
  }

  private case class PersistSingleEvent[A](event: A, handler: (A) => Unit)
}
