package pl.edu.bit.membership.infrastructure.net.spray

import spray.http.HttpResponse
import spray.json._

import scala.util.Try

/**
 * @author Wojciech Milewski
 */
object HttpResponseToResponseConversions {
  implicit class HttpResponseConverter(httpResponse: HttpResponse) {
    def as[T](implicit jsonFormat: JsonFormat[T]): Option[T] = {
      if (httpResponse.entity.isEmpty)
        throw new IllegalArgumentException("Empty body cannot be converted")
      Try(httpResponse.entity.asString.parseJson.convertTo[T]).toOption
    }
  }
}
