package pl.edu.bit.membership.infrastructure.net.spray

/**
 * @author Wojciech Milewski
 */
case class RequestContext(authToken: String)

object RequestContext {
  val TOKEN_HEADER = "knbit-aa-auth"
}
