package pl.edu.bit.membership.readmodel

import akka.actor.{Props, Actor, ActorLogging}
import pl.edu.bit.membership.domain.event.{DeclarationAccepted, DeclarationRejected, DeclarationSubmitted}
import pl.edu.bit.membership.readmodel.db.{UserDetailsDao, UserDetailsEntity, UserDetailsState}

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
 * @author Wojciech Milewski
 */
class UserDetailsAggregator(implicit dao: UserDetailsDao) extends Actor with ActorLogging {

  override def receive: Receive = {
    case ev: DeclarationSubmitted =>
      dao.save(new UserDetailsEntity(ev.userId, ev.email, ev.firstName, ev.lastName, ev.departmentName, ev.indexNumber.value, ev.startOfStudiesYear.toString, ev.submitDate, ev.expirationDate)).onComplete {
        case Success(_) =>
        case Failure(e) => log.error(e, "exception during read model update")
      }
    case ev: DeclarationAccepted =>
      dao.load(ev.userId.id)
        .flatMap(opt => Future(opt.getOrElse(throw new NoSuchElementException(ev.userId.id))))
        .map(entity => entity.setState(UserDetailsState.ACCEPTED))
        .flatMap(dao.save)
        .onComplete {
        case Success(_) =>
        case Failure(e) => log.error(e, "exception during read model update")
      }
    case ev: DeclarationRejected =>
      dao.load(ev.userId.id)
        .flatMap(opt => Future(opt.getOrElse(throw new NoSuchElementException(ev.userId.id))))
        .map(entity => entity.setState(UserDetailsState.REJECTED))
        .flatMap(dao.save)
        .onComplete {
        case Success(_) =>
        case Failure(e) => log.error(e, "exception during read model update")
      }

  }
}

object UserDetailsAggregator {
  def props(implicit dao: UserDetailsDao): Props = Props(new UserDetailsAggregator)
}
