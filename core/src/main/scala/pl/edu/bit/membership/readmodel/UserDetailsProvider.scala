package pl.edu.bit.membership.readmodel

import pl.edu.bit.membership.domain.valueobject.id.UserId
import pl.edu.bit.membership.readmodel.db.{UserDetailsDao, UserDetailsEntity}

import scala.concurrent.Future
import scala.collection.immutable

/**
 * @author Wojciech Milewski
 */
class UserDetailsProvider(implicit dao: UserDetailsDao) {

  def get(userId: UserId): Future[Option[UserDetailsEntity]] = dao.load(userId.id)

  def getSeq(ids: immutable.Seq[UserId]): Future[immutable.Seq[UserDetailsEntity]] = dao.loadSeq(ids.map(_.toString))

  def getAll: Future[immutable.Seq[UserDetailsEntity]] = dao.loadAll()
}
