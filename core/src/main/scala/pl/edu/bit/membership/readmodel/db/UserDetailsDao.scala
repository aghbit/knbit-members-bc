package pl.edu.bit.membership.readmodel.db

import java.time.LocalDate

import com.mongodb.casbah.MongoCollection
import com.mongodb.casbah.commons.MongoDBObject
import pl.edu.bit.membership.domain.valueobject.Email
import pl.edu.bit.membership.domain.valueobject.id.UserId

import scala.collection.JavaConverters._
import scala.collection.immutable
import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future

import com.mongodb.casbah.Imports._

import Fields._

/**
 * @author Wojciech Milewski
 */
class UserDetailsDao(implicit db: MongoCollection) {

  def load(userId: String): Future[Option[UserDetailsEntity]] =
    Future(db.findOneByID(userId).map(convert))

  def loadSeq(ids: immutable.Seq[String]): Future[immutable.Seq[UserDetailsEntity]] = Future.traverse(ids)(load).map(seq => seq.filter(_.isDefined).map(_.get))

  def loadAll(): Future[immutable.Seq[UserDetailsEntity]] =
    Future(Vector() ++ db.find().map(convert))

  def save(entity: UserDetailsEntity): Future[Unit] =
    Future(db.save(MongoDBObject(
      id -> entity.userId.id,
      email -> entity.email.value,
      firstName -> entity.firstName,
      lastName -> entity.lastName,
      departmentName -> entity.departmentName,
      indexNumber -> entity.indexNumber,
      startOfStudiesYear -> entity.startOfStudiesYear,
      submitDate -> entity.submitDate.toEpochDay,
      expirationDate -> entity.expirationDate.toEpochDay,
      state -> entity.state
    )))

  private def convert(o: DBObject): UserDetailsEntity = {
    UserDetailsEntity(
      UserId(o.getAs[String](id).get),
      Email(o.getAs[String](email).get),
      o.getAs[String](firstName).get,
      o.getAs[String](lastName).get,
      o.getAs[String](departmentName).get,
      o.getAs[String](indexNumber).get,
      o.getAs[String](startOfStudiesYear).get,
      LocalDate.ofEpochDay(o.getAs[Long](submitDate).get),
      LocalDate.ofEpochDay(o.getAs[Long](expirationDate).get),
      o.getAs[String](state).get
    )
  }

}

private object Fields {
  val id = "_id"
  val email = "email"
  val firstName = "firstName"
  val lastName = "lastName"
  val departmentName = "departmentName"
  val indexNumber = "indexNumber"
  val startOfStudiesYear = "startOfStudiesYear"
  val submitDate = "submitDate"
  val expirationDate = "expirationDate"
  val state = "state"

}
