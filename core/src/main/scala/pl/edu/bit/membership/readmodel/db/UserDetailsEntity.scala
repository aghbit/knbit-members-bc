package pl.edu.bit.membership.readmodel.db

import java.time.LocalDate

import pl.edu.bit.membership.domain.valueobject.Email
import pl.edu.bit.membership.domain.valueobject.id.UserId

/**
 * @author Wojciech Milewski
 */
case class UserDetailsEntity(
                              userId: UserId,
                              email: Email,
                              firstName: String,
                              lastName: String,
                              departmentName: String,
                              indexNumber: String,
                              startOfStudiesYear: String,
                              submitDate: LocalDate,
                              expirationDate: LocalDate,
                              state: String = UserDetailsState.CREATED
                              ) {
  def setState(state: String): UserDetailsEntity = {
    require(state == UserDetailsState.CREATED || state == UserDetailsState.ACCEPTED || state == UserDetailsState.REJECTED)
    this.copy(state = state)
  }

}

object UserDetailsState {
  val CREATED = "created"
  val ACCEPTED = "accepted"
  val REJECTED = "rejected"
}