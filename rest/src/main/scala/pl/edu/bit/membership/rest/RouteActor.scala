package pl.edu.bit.membership.rest

import akka.actor.Props
import pl.edu.bit.membership.rest.config.{CorsDirective, CorsRoute}
import pl.edu.bit.membership.rest.controller.{CreateAccountController, MemberReadController, MemberWriteController}
import spray.routing.HttpServiceActor

/**
 * @author Wojciech Milewski
 */
class RouteActor(
                createAccountController: CreateAccountController,
                memberWriteController: MemberWriteController,
                memberReadController: MemberReadController
                  ) extends HttpServiceActor with CorsDirective with CorsRoute {
  override def receive: Receive = runRoute {
    corsRoute ~ createAccountController.route ~ memberWriteController.route ~ memberReadController.route
  }
}

object RouteActor {
  def props(
           createAccountController: CreateAccountController,
           memberWriteController: MemberWriteController,
           memberReadController: MemberReadController
             ): Props = Props(new RouteActor(createAccountController, memberWriteController, memberReadController))
}
