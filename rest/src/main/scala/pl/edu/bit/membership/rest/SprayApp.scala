package pl.edu.bit.membership.rest

import akka.actor.ActorSystem
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import com.mongodb.casbah.Imports._
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging
import pl.edu.bit.membership.domain.event.{DeclarationAccepted, DeclarationRejected, DeclarationSubmitted}
import pl.edu.bit.membership.domain.office.{AdminOffice, MemberOffice}
import pl.edu.bit.membership.external.aa.{MockedAuthService, AuthService, HttpAuthService}
import pl.edu.bit.membership.readmodel.db.UserDetailsDao
import pl.edu.bit.membership.readmodel.{UserDetailsAggregator, UserDetailsProvider}
import pl.edu.bit.membership.rest.controller.{CreateAccountController, MemberReadController, MemberWriteController}
import spray.can.Http

import scala.concurrent.duration._
import scala.util.control.NonFatal

/**
 * @author Wojciech Milewski
 */
object SprayApp extends App with StrictLogging {
  // we need an ActorSystem to host our application in

  try {
    val config = resolveConfig()

    implicit val system = ActorSystem("membership", config)

    // WRITE MODEL INITIALIZAION

    // external services
    val authServiceAddress = config.getString("membership.aa.address")
    logger.info(s"Auth service address: $authServiceAddress")

    implicit val authService: AuthService = getAuthService(resolveCommunicationState(config))(authServiceAddress)

    // offices
    val adminOffice = AdminOffice()
    val memberOffice = MemberOffice()

    // READ MODEL INITIALIZATION
    val mongoAddress = config.getString("membership.read.mongo.address")
    val readModelDbName = config.getString("membership.read.mongo.db")
    implicit val mongoCollection = MongoClient(mongoAddress)(readModelDbName)("userdetailsentity")
    implicit val userDetailsDao = new UserDetailsDao
    val readmodelActor = system.actorOf(UserDetailsAggregator.props)
    system.eventStream.subscribe(readmodelActor, classOf[DeclarationSubmitted])
    system.eventStream.subscribe(readmodelActor, classOf[DeclarationAccepted])
    system.eventStream.subscribe(readmodelActor, classOf[DeclarationRejected])

    implicit val userDetailsProvider = new UserDetailsProvider()

    // Controllers
    val createAccountController = CreateAccountController(adminOffice)
    val memberController = MemberWriteController(memberOffice)
    val memberReadController = new MemberReadController

    // Main routing actor
    val routeActor = system.actorOf(RouteActor.props(createAccountController, memberController, memberReadController))

    // binding routing actor as server
    implicit val timeout = Timeout(5.seconds)
    // start a new HTTP server on port 8080 with our service actor as the handler
    val interface = config.getString("membership.rest.interface")
    val port = config.getInt("membership.rest.port")
    IO(Http) ? Http.Bind(routeActor, interface = interface, port = port)
  }
  catch {
    case NonFatal(e) =>
      logger.error("Cannot start KNBIT-MEMBERSHIP", e)
      System.exit(1)
  }

  private def resolveConfig(): Config = {
    args.foreach(config => logger.debug(s"Used config: $config"))

    args
      .map(dirName => ConfigFactory.parseResources(s"config/$dirName/application.conf"))
      .foldLeft(ConfigFactory.empty())((collector, config) => collector.withFallback(config))
      .withFallback(ConfigFactory.load("config/default/application"))
  }

  private def resolveCommunicationState(config: Config) = {
    val value = config.getString("membership.communication")
    if (value == "ON") CommunicationOn
    else if (value == "OFF") CommunicationOff
    else throw new IllegalArgumentException(s"Incorrect Communication state: $value")
  }

  private def getAuthService(communicationState: CommunicationState)(address: String)(implicit system: ActorSystem): AuthService = communicationState match {
    case CommunicationOn => HttpAuthService(address)
    case CommunicationOff => MockedAuthService()
  }
}

sealed trait CommunicationState
case object CommunicationOn extends CommunicationState
case object CommunicationOff extends CommunicationState
