package pl.edu.bit.membership.rest.config

import pl.edu.bit.membership.infrastructure.net.spray.RequestContext
import spray.http.HttpHeaders.RawHeader
import spray.routing.directives.{RouteDirectives, MethodDirectives, RespondWithDirectives}

/**
 * @author Wojciech Milewski
 */
trait CorsDirective extends RespondWithDirectives with MethodDirectives with RouteDirectives {

  private val origin = RawHeader("Access-Control-Allow-Origin", "*")
  private val credentials = RawHeader("Access-Control-Allow-Credentials", "true")
  private val methods = RawHeader("Access-Control-Allow-Methods", "POST, PUT, GET, HEAD, OPTIONS, PATCH")
  private val headers = RawHeader("Access-Control-Allow-Headers", RequestContext.TOKEN_HEADER + ", Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")

  def allowCors = respondWithHeaders(origin, credentials, methods, headers)
}
