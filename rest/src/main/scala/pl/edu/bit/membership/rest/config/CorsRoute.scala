package pl.edu.bit.membership.rest.config

import spray.routing.directives.{MethodDirectives, RespondWithDirectives, RouteDirectives}

/**
 * @author Wojciech Milewski
 */
trait CorsRoute extends CorsDirective with RespondWithDirectives with MethodDirectives with RouteDirectives{
  val corsRoute = allowCors {
    options {
      complete("")
    }
  }
}
