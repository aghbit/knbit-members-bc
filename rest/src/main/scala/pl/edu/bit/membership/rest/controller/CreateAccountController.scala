package pl.edu.bit.membership.rest.controller

import akka.actor.{ActorRef, ActorSystem}
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.scalalogging.StrictLogging
import pl.edu.bit.membership.domain.command.CreateAccount
import pl.edu.bit.membership.domain.event.AccountRejected.{InvalidEmail, PasswordTooWeak, UserExists}
import pl.edu.bit.membership.domain.event.{AccountCreated, AccountRejected}
import pl.edu.bit.membership.domain.valueobject.{Email, Password}
import pl.edu.bit.membership.rest.config.CorsDirective
import pl.edu.bit.membership.rest.json.Protocol._
import pl.edu.bit.membership.rest.json.{BadRequestResponseJson, CreateAccountRequestJson, CreateAccountResponseJson}
import spray.http.MediaTypes._
import spray.http._
import spray.routing.HttpService

import scala.concurrent.duration._
import scala.util.{Failure, Success}


/**
 * @author Wojciech Milewski
 */
class CreateAccountController(adminOffice: ActorRef)(implicit system: ActorSystem) extends HttpService with CorsDirective with StrictLogging {

  def actorRefFactory = system

  val route =
    allowCors {
      path("account") {
        post {
          entity(as[CreateAccountRequestJson]) { json =>
            import system.dispatcher
            implicit val timeout = Timeout(5.seconds)
            onComplete(adminOffice ? CreateAccount(Email(json.email), Password(json.password))) {
              case Success(AccountCreated(email, userid)) => respondWithMediaType(`application/json`) {
                complete(CreateAccountResponseJson(userid.id))
              }
              case Success(AccountRejected(email, InvalidEmail)) =>
                complete(StatusCodes.BadRequest, BadRequestResponseJson("INVALID_EMAIL"))
              case Success(AccountRejected(email, PasswordTooWeak)) =>
                complete(StatusCodes.BadRequest, BadRequestResponseJson("PASSWORD_TOO_WEAK"))
              case Success(AccountRejected(email, UserExists)) =>
                complete(StatusCodes.BadRequest, BadRequestResponseJson("USER_EXISTS"))
              case Success(_) => complete(StatusCodes.BadRequest, BadRequestResponseJson("UNKNOWN_ERROR"))
              case Failure(e) =>
                logger.error("failure", e)
                complete(StatusCodes.BadRequest, BadRequestResponseJson("UNKNOWN_ERROR"))
            }
          }
        }
      }

    }
}

object CreateAccountController {
  def apply(adminOffice: ActorRef)(implicit system: ActorSystem) = new CreateAccountController(adminOffice)
}
