package pl.edu.bit.membership.rest.controller

import akka.actor.{ActorRefFactory, ActorSystem}
import akka.util.Timeout
import com.typesafe.scalalogging.StrictLogging
import pl.edu.bit.membership.domain.valueobject.id.UserId
import pl.edu.bit.membership.readmodel.UserDetailsProvider
import pl.edu.bit.membership.rest.config.CorsDirective
import pl.edu.bit.membership.rest.json.Protocol._
import pl.edu.bit.membership.rest.json.{UserDetailsJson, UserDetailsSeqJson}
import spray.http.StatusCodes
import spray.routing.HttpService

import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
 * @author Wojciech Milewski
 */
class MemberReadController(implicit detailsProvider: UserDetailsProvider, system: ActorSystem) extends HttpService with CorsDirective with StrictLogging {
  def actorRefFactory: ActorRefFactory = system

  private val getOne =
    path("users" / Rest) { userId =>
      get {
        import system.dispatcher
        implicit val timeout = Timeout(2.seconds)
        onComplete(detailsProvider.get(UserId(userId))) {
          case Success(Some(e)) =>
            complete(StatusCodes.OK, UserDetailsJson(
              e.userId.id,
              e.email.value,
              e.firstName,
              e.lastName,
              e.departmentName,
              e.indexNumber,
              e.startOfStudiesYear,
              e.submitDate.toEpochDay,
              e.expirationDate.toEpochDay,
              e.state
            ))
          case Success(None) => complete(StatusCodes.NotFound)
          case Failure(e) =>
            logger.error("GET /users", e)
            complete(StatusCodes.BadRequest)
        }
      }

    }

  private val getAll =
    path("users") {
      get {
        import system.dispatcher
        implicit val timeout = Timeout(2.seconds)
        onComplete(detailsProvider.getAll) {
          case Success(seq) =>
            complete(StatusCodes.OK, UserDetailsSeqJson(seq.map(e => UserDetailsJson(
              e.userId.id,
              e.email.value,
              e.firstName,
              e.lastName,
              e.departmentName,
              e.indexNumber,
              e.startOfStudiesYear,
              e.submitDate.toEpochDay,
              e.expirationDate.toEpochDay,
              e.state
            ))))
          case Failure(e) =>
            logger.error("GET /users", e)
            complete(StatusCodes.BadRequest)
        }
      }
    }

  val route = allowCors {
    getOne ~ getAll
  }
}

object MemberReadController {
  def apply(implicit detailsProvider: UserDetailsProvider, system: ActorSystem) = new MemberReadController
}
