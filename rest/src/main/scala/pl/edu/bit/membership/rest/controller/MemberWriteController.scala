package pl.edu.bit.membership.rest.controller

import akka.actor.{ActorRef, ActorSystem}
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.scalalogging.StrictLogging
import pl.edu.bit.membership.domain.command.{AcceptDeclaration, RejectDeclaration, SubmitDeclaration}
import pl.edu.bit.membership.domain.event.{DeclarationAccepted, DeclarationRejected}
import pl.edu.bit.membership.domain.valueobject.id.UserId
import pl.edu.bit.membership.domain.valueobject.{Email, IndexNumber}
import pl.edu.bit.membership.infrastructure.net.spray.RequestContext
import pl.edu.bit.membership.rest.config.CorsDirective
import pl.edu.bit.membership.rest.json.Protocol._
import pl.edu.bit.membership.rest.json.{AcceptDeclarationRequestJson, BadRequestResponseJson, RejectDeclarationRequestJson, SubmitDeclarationRequestJson}
import spray.http.StatusCodes
import spray.routing.HttpService

import scala.concurrent.duration._
import scala.util.{Failure, Success}


/**
 * @author Wojciech Milewski
 */
class MemberWriteController(memberOffice: ActorRef)(implicit system: ActorSystem) extends HttpService with CorsDirective with StrictLogging {

  def actorRefFactory = system

  val submitDeclaration =
    path("declaration" / "submit") {
      post {
        entity(as[SubmitDeclarationRequestJson]) { json =>
          if (!Email(json.userId).isValid) complete(StatusCodes.BadRequest, BadRequestResponseJson("INVALID_USERID"))
          if (!Email(json.email).isValid) complete(StatusCodes.BadRequest, BadRequestResponseJson("INVALID_EMAIL"))
          if (!IndexNumber(json.indexNumber.toString).isValid) complete(StatusCodes.BadRequest, BadRequestResponseJson("INVALID_INDEX_NUMBER"))
          memberOffice ! SubmitDeclaration(
            UserId(json.userId),
            Email(json.email),
            json.firstName,
            json.lastName,
            json.departmentName,
            IndexNumber(json.indexNumber.toString),
            json.startOfStudiesYear
          )
          complete(StatusCodes.NoContent)
        }
      }
    }

  val acceptDeclaration =
    path("declaration" / "accept") {
      post {
        headerValueByName(RequestContext.TOKEN_HEADER) { tokenHeader =>
          entity(as[AcceptDeclarationRequestJson]) { json =>
            import system.dispatcher
            implicit val timeout = Timeout(2 seconds)
            implicit val context = RequestContext(tokenHeader)
            onComplete(memberOffice ? AcceptDeclaration(UserId(json.userId))) {
              case Success(DeclarationAccepted(userId)) => complete(StatusCodes.NoContent)
              case Success(_) => complete(StatusCodes.BadRequest)
              case Failure(e) =>
                logger.error("POST declaration/accept", e)
                complete(StatusCodes.BadRequest)
            }
          }
        }
      }
    }

  val rejectDeclaration =
    path("declaration" / "reject") {
      post {
        entity(as[RejectDeclarationRequestJson]) { json =>
          import system.dispatcher
          implicit val timeout = Timeout(2 seconds)
          onComplete(memberOffice ? RejectDeclaration(UserId(json.userId))) {
            case Success(DeclarationRejected(userId)) => complete(StatusCodes.NoContent)
            case Success(_) => complete(StatusCodes.BadRequest)
            case Failure(e) =>
              logger.error("POST declaration/reject", e)
              complete(StatusCodes.BadRequest)
          }

        }
      }
    }

  val route = allowCors {
    submitDeclaration ~ acceptDeclaration ~ rejectDeclaration
  }
}

object MemberWriteController {
  def apply(memberOffice: ActorRef)(implicit system: ActorSystem) = new MemberWriteController(memberOffice)
}