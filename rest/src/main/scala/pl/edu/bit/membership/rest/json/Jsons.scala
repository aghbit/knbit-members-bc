package pl.edu.bit.membership.rest.json

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

import scala.collection.immutable
/**
 * @author Wojciech Milewski
 */
sealed trait RequestJson
case class CreateAccountRequestJson(email: String, password: String) extends RequestJson
case class SubmitDeclarationRequestJson(
                                         userId: String,
                                         email: String,
                                         firstName: String,
                                         lastName: String,
                                         departmentName: String,
                                         indexNumber: Int,
                                         startOfStudiesYear: Int) extends RequestJson
case class AcceptDeclarationRequestJson(
                                       userId: String
                                         ) extends RequestJson
case class RejectDeclarationRequestJson(
                                       userId: String
                                         ) extends RequestJson

sealed trait ResponseJson
case class CreateAccountResponseJson(userId: String) extends ResponseJson
case class UserDetailsJson(
                            userId: String,
                            email: String,
                            firstName: String,
                            lastName: String,
                            departmentName: String,
                            indexNumber: String,
                            startOfStudiesYear: String,
                            submitDateEpochDay: Long,
                            expirationDateEpochDay: Long,
                            state: String) extends ResponseJson
case class UserDetailsSeqJson(values: immutable.Seq[UserDetailsJson]) extends ResponseJson
case class BadRequestResponseJson(reason: String) extends ResponseJson

object Protocol extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val createAccountRequest = jsonFormat2(CreateAccountRequestJson)
  implicit val submitDeclarationRequest = jsonFormat7(SubmitDeclarationRequestJson)
  implicit val acceptDeclarationRequest = jsonFormat1(AcceptDeclarationRequestJson)
  implicit val rejectDeclarationRequest = jsonFormat1(RejectDeclarationRequestJson)

  implicit val createAccountResponse = jsonFormat1(CreateAccountResponseJson)
  implicit val userDetailsResponse = jsonFormat10(UserDetailsJson)
  implicit val userDetailsSeqResponse = jsonFormat1(UserDetailsSeqJson)
  implicit val badRequestResponse = jsonFormat1(BadRequestResponseJson)
}
